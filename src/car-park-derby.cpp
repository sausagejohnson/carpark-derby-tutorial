/**
 * @file car-park-derby.cpp
 * @date 16-Jun-2023
 */

#define __SCROLL_IMPL__
#include "car-park-derby.h"
#undef __SCROLL_IMPL__

#include "Car.h"

#ifdef __orxMSVC__

/* Requesting high performance dedicated GPU on hybrid laptops */
__declspec(dllexport) unsigned long NvOptimusEnablement        = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

#endif // __orxMSVC__

//orxOBJECT *car;
ScrollObject *car;


/** Update function, it has been registered to be called every tick of the core clock
 */
void car_park_derby::Update(const orxCLOCK_INFO &_rstInfo)
{
    // Should quit?
    if(orxInput_IsActive("Quit"))
    {
        // Send close event
        orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
    }
    
	orxOBJECT *carObject = car->GetOrxObject();
	
	orxCAMERA *camera = GetMainCamera();
	
	orxVECTOR cameraPosition = {};
	orxCamera_GetPosition(camera, &cameraPosition);
 
	orxVECTOR carPosition = {};
	orxObject_GetPosition(carObject, &carPosition);
 
	cameraPosition.fX = carPosition.fX;
	cameraPosition.fY = carPosition.fY;
	orxCamera_SetPosition(camera, &cameraPosition);
    orxSoundSystem_SetListenerPosition(0, &cameraPosition);
	
	this->ReduceTime();
}

/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS car_park_derby::Init()
{
    // Display a small hint in console
    orxLOG("\n* This template project creates a simple scene"
    "\n* You can play with the config parameters in ../data/config/car-park-derby.ini"
    "\n* After changing them, relaunch the executable to see the changes.");

    // Create the scene
    CreateObject("Scene");
    
    float offset = 3072;
    for (float x=0; x<25; x++){
        for (float y=0; y<25; y++){
            orxVECTOR position = { (x*256)-offset, (y*256)-offset, 0.0 };
            orxOBJECT *tile = orxObject_CreateFromConfig("RoadTile");
            orxObject_SetPosition(tile, &position);
        }
    }
    
	car = CreateObject("Car");

	Car *botCar = CreateObject<Car>("Car");
	botCar->SetAsBot();
    botCar = CreateObject<Car>("Car");
	botCar->SetAsBot();
    botCar = CreateObject<Car>("Car");
	botCar->SetAsBot();
	
	//create a barrier border
	float barrierOffset = 2304;
	for (int sides=0; sides<1; sides++){
		for (float x=0; x<19; x++){
            orxVECTOR position = { (x*256)-barrierOffset, -barrierOffset, -0.1 };
            orxOBJECT *barrier = orxObject_CreateFromConfig("Barrier");
            orxObject_SetPosition(barrier, &position);
			orxVECTOR position2 = { (x*256)-barrierOffset, barrierOffset, -0.1 };
            orxOBJECT *barrier2 = orxObject_CreateFromConfig("Barrier");
            orxObject_SetPosition(barrier2, &position2);
        }
		for (float y=1; y<18; y++){
            orxVECTOR position = { -barrierOffset, (y*256)-barrierOffset, -0.1 };
            orxOBJECT *barrier = orxObject_CreateFromConfig("Barrier");
            orxObject_SetPosition(barrier, &position);
			orxVECTOR position2 = { barrierOffset, (y*256)-barrierOffset, -0.1 };
            orxOBJECT *barrier2 = orxObject_CreateFromConfig("Barrier");
            orxObject_SetPosition(barrier2, &position2);
        }
	}
	
    // Done!
    return orxSTATUS_SUCCESS;
}

/** Run function, it should not contain any game logic
 */
orxSTATUS car_park_derby::Run()
{
    // Return orxSTATUS_FAILURE to instruct orx to quit
    return orxSTATUS_SUCCESS;
}

/** Exit function, it is called before exiting from orx
 */
void car_park_derby::Exit()
{
    // Let orx clean all our mess automatically. :)
}

/** BindObjects function, ScrollObject-derived classes are bound to config sections here
 */
void car_park_derby::BindObjects()
{
    // Bind the Object class to the Object config section
    ScrollBindObject<Car>("Car");
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS car_park_derby::Bootstrap() const
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

int car_park_derby::GetTime(){
    orxConfig_PushSection("Runtime");
	int time = orxConfig_GetU64("CurrentTime");
	orxConfig_PopSection();
    
    return time;
}

void car_park_derby::AddToScore(int points){

	orxConfig_PushSection("Runtime");
	int score = orxConfig_GetU64("CurrentScore");
	score += points;
	orxConfig_SetU64("CurrentScore", score);
	orxConfig_PopSection();

}

void car_park_derby::ReduceTime(){
	orxConfig_PushSection("Runtime");
	int time = orxConfig_GetU64("CurrentTime");
	
    if (time > 0){
        time--;
        orxConfig_SetU64("CurrentTime", time);
    }
	orxConfig_PopSection();
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Execute our game
    car_park_derby::GetInstance().Execute(argc, argv);

    // Done!
    return EXIT_SUCCESS;
}
