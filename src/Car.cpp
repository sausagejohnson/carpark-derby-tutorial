#include "Car.h"

void Car::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
	ScrollObject *skidMaker = this->GetSkidMaker();
	skidMaker->Enable(false, false);
}

void Car::OnDelete()
{
}

orxBOOL Car::OnCollide(ScrollObject *_poCollider, orxBODY_PART *_pstPart, orxBODY_PART *_pstColliderPart, const orxVECTOR &_rvPosition, const orxVECTOR &_rvNormal){  
    const orxSTRING colliderName = _poCollider->GetModelName();
    
    const orxSTRING partOne = orxBody_GetPartName(_pstColliderPart);
    const orxSTRING partTwo = orxBody_GetPartName(_pstPart);
    

    
    if (orxString_SearchString(partOne, "CarBodyPart") != orxNULL && 
    orxString_SearchString(partTwo, "CarBodyPart") != orxNULL) {
        
        if (!this->isBot){
            orxVECTOR speedVector = {};
            this->GetSpeed(speedVector);
            
            orxFLOAT vectorSize = orxVector_GetSize(&speedVector);

            car_park_derby::GetInstance().AddToScore(vectorSize);
        }
        
    }
       
    this->AddSound("Collision");
    controlLossFrames = 60; //concussion time.

	return orxTRUE;
}

ScrollObject* Car::GetSkidMaker() {
 
	for (ScrollObject *child = this->GetOwnedChild(); child; child = child->GetOwnedSibling()) {
		if (orxString_Compare(child->GetModelName(), "SkidMaker") == 0) {
			return child;
		}
	}
 
    return orxNULL;
}

void Car::SetAsBot(){
	this->isBot = true;
	if (this->isBot){
        orxFLOAT randomX = orxMath_GetRandomFloat(-2000, 2000); 
        orxFLOAT randomY = orxMath_GetRandomFloat(-2000, 2000); 

		orxVECTOR randomPosition = {randomX, randomY, -0.1};
		this->SetPosition(randomPosition);
		        
        botTurnSpeed = orxMath_GetRandomU32(0, 1) == 1 ? -1.2 : 1.2; 
	}
}

void Car::Update(const orxCLOCK_INFO &_rstInfo)
{
        
    if (controlLossFrames > 0 ){
        this->UpdateMotorPitch();
		this->ApplySkid();
        controlLossFrames--;
        return;
    }
    
    if (car_park_derby::GetInstance().GetTime() == 0){
        this->UpdateMotorPitch();
        this->ApplySkid();
        return;
    }
        
	if (this->isBot){
		this->Accelerate(100);
		//this->TurnCar(-1.2);
        this->TurnCar( botTurnSpeed );
	} else {
				
		if (orxInput_IsActive("Accelerate")){
			this->Accelerate(100);
		}
		
		if (orxInput_IsActive("Reverse")){
			this->Accelerate(-30);
		}
		
		if(orxInput_IsActive("TurnLeft"))
		{
			this->TurnCar(-2);
		}
		
		
		if(orxInput_IsActive("TurnRight"))
		{
			this->TurnCar(2);
		}
		
		
	}
	
	this->UpdateMotorPitch();
	this->ApplySkid();
	
}

void Car::ApplySkid(){

	orxOBJECT *carObject = this->GetOrxObject();
	
	orxFLOAT rot = orxObject_GetAngularVelocity(carObject);	
	rot = orxMath_Abs(rot);
	
	ScrollObject *skidMaker = this->GetSkidMaker();
	
	if (rot > 2.0){
		skidMaker->Enable(true, false);
	} else {
		skidMaker->Enable(false, false);
	}
}

//calc the amount of turn based on the speed of the car.
//Speed is 0 - 2000
//radians turn is 1 - 2
//100% = 2000/20
//50%  = 1000/20
//25%  = 500/20
//percent/100 = 0.5 : 1+0.5 = 1.5
//100/100 = 1 : 1+1 = 2
void Car::TurnCar(orxFLOAT radiansMultiplier){
	orxVECTOR direction = {};
	this->GetSpeed(direction, true); //true = relative
	
	orxFLOAT speed = orxVector_GetSize(&direction);
	
	orxFLOAT speedPercent = speed / 20;
	orxFLOAT radiansRatio = (speedPercent / 100);
	
	orxFLOAT radiansPerSecond = radiansRatio * radiansMultiplier; //-2 or 2;
	orxOBJECT *carObject;
	carObject = this->GetOrxObject();
	orxObject_SetAngularVelocity(carObject, radiansPerSecond);    
}

void Car::Accelerate(orxFLOAT strength){

	orxVECTOR power = { 0, -strength, 0};

	orxVECTOR direction = {};
	this->GetSpeed(direction, true); //true = relative
	
	orxFLOAT size = orxVector_GetSize(&direction);
	
	if (size < 2000){
		orxVector_Add(&direction, &direction, &power);
		this->SetSpeed(direction, true); //true = relative
	}
	


}

void Car::UpdateMotorPitch(){
	orxVECTOR direction = {};
	this->GetSpeed(direction, true); //true = relative
	
	orxFLOAT size = orxVector_GetSize(&direction);
	
	orxFLOAT sizeFraction = size/2000;
	if (sizeFraction < 0.2) sizeFraction = 0.2; //minimal low engine noise
	
	orxOBJECT *carObject = this->GetOrxObject();
	orxObject_SetPitch(carObject, sizeFraction); 
}
