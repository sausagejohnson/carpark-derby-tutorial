##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=car-park-derby
ConfigurationName      :=Debug_x64
WorkspaceConfiguration :=Debug_x64
WorkspacePath          :=C:/Work/orx-projects/carpark-derby-tutorial/build/windows/codelite
ProjectPath            :=C:/Work/orx-projects/carpark-derby-tutorial/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Wayne Johnson
Date                   :=23/06/2023
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/mingw64/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/mingw64/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputDirectory        :=../../../bin
OutputFile             :=../../../bin/car-park-derbyd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="car-park-derby.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/mingw64/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overridden using an environment variable
##
AR       := C:/mingw64/mingw64/bin/ar.exe rcu
CXX      := C:/mingw64/mingw64/bin/g++.exe
CC       := C:/mingw64/mingw64/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/mingw64/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Car.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y C:\Work\orx\code\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(ObjectSuffix): ../../../src/car-park-derby.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(DependSuffix) -MM ../../../src/car-park-derby.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/orx-projects/carpark-derby-tutorial/src/car-park-derby.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(PreprocessSuffix): ../../../src/car-park-derby.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_car-park-derby.cpp$(PreprocessSuffix) ../../../src/car-park-derby.cpp

$(IntermediateDirectory)/up_up_up_src_Car.cpp$(ObjectSuffix): ../../../src/Car.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Car.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Car.cpp$(DependSuffix) -MM ../../../src/Car.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/orx-projects/carpark-derby-tutorial/src/Car.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Car.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Car.cpp$(PreprocessSuffix): ../../../src/Car.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Car.cpp$(PreprocessSuffix) ../../../src/Car.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


