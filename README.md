# Carpark Derby (tutorial version)


## The Video Tutorial

This will be the final project files resulting from the video tutorial (pending). You can learn to make this game.

## Installation
You can clone this repo with:

git clone https://gitlab.com/sausagejohnson/carpark-derby-tutorial.git

A zip will follow when the code is complete.

## Technology

This game is written in C++ using the [Orx Portal Game Engine](https://orx-project.org).