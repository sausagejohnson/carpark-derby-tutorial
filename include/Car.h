/**
 * @file Object.h
 * @date 16-Jun-2023
 */

#ifndef __CAR_H__
#define __CAR_H__

#include "car-park-derby.h"

/** Car Class
 */
class Car : public ScrollObject
{
public:
				void			SetAsBot();				
		
protected:

                void            OnCreate();
                void            OnDelete();
                orxBOOL         OnCollide(ScrollObject *_poCollider, orxBODY_PART *_pstPart, orxBODY_PART *_pstColliderPart, const orxVECTOR &_rvPosition, const orxVECTOR &_rvNormal);
                void            Update(const orxCLOCK_INFO &_rstInfo);
				ScrollObject* 	GetSkidMaker();
				void			TurnCar(orxFLOAT radiansMultiplier);
				void 			Accelerate(orxFLOAT strength);
				void			UpdateMotorPitch();
				void			ApplySkid();

private:

				orxBOOL			isBot = orxFALSE;
                orxFLOAT        botTurnSpeed = 0;
                int             controlLossFrames = 0;
};

#endif // __CAR_H__
