/**
 * @file car-park-derby.h
 * @date 16-Jun-2023
 */

#ifndef __car_park_derby_H__
#define __car_park_derby_H__

#define __NO_SCROLLED__
#include "Scroll.h"

/** Game Class
 */
class car_park_derby : public Scroll<car_park_derby>
{
public:
				void			AddToScore(int points);
				void			ReduceTime();
                int             GetTime();

private:

                orxSTATUS       Bootstrap() const;

                void            Update(const orxCLOCK_INFO &_rstInfo);

                orxSTATUS       Init();
                orxSTATUS       Run();
                void            Exit();
                void            BindObjects();

};

#endif // __car_park_derby_H__
